#include "se3_pose_pose_offset_quaternion_error_factor_ad.h"
#include <srrg_solver/solver_core/ad_error_factor_impl.cpp>
#include <srrg_solver/solver_core/error_factor_impl.cpp>
#include <srrg_solver/solver_core/instance_macros.h>

namespace srrg2_solver_extras {

  void SE3PosePoseOffsetQuaternionErrorFactorAD::_drawImpl(ViewerCanvasPtr canvas_) const {
  }

  INSTANTIATE(SE3PosePoseOffsetQuaternionErrorFactorAD)
} // namespace srrg2_solver_extras
