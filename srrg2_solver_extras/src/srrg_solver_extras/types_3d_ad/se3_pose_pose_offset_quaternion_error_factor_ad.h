#pragma once
#include <srrg_solver/solver_core/ad_error_factor.h>
#include <srrg_solver/variables_and_factors/types_3d/variable_se3_ad.h>

namespace srrg2_solver_extras {
  using namespace srrg2_core;
  using namespace srrg2_solver;

  class SE3PosePoseOffsetQuaternionErrorFactorAD
    : public ADErrorFactor_<6, VariableSE3QuaternionLeftAD, VariableSE3QuaternionLeftAD>,
      public MeasurementOwnerEigen_<Isometry3f> {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    using BaseType = ADErrorFactor_<6, VariableSE3QuaternionLeftAD, VariableSE3QuaternionLeftAD>;
    using VariableTupleType = typename BaseType::VariableTupleType;

    BaseType::ADErrorVectorType operator()(VariableTupleType& vars) final {
      this->_is_valid                         = true;
      const Isometry3_<DualValuef>& Xi        = vars.at<0>()->adEstimate();
      const Isometry3_<DualValuef>& Xj        = vars.at<1>()->adEstimate();
      const Isometry3_<DualValuef> prediction = Xi * _T * Xj;
      return geometry3d::t2v(_inverse_measurement * prediction);
    }

    void setMeasurement(const Isometry3f& meas_) override {
      _measurement = meas_;
      convertMatrix(_inverse_measurement, meas_.inverse());
    }

    void setTransform(const Isometry3f& T_) {
      convertMatrix(_T, T_);
    }

    void serialize(srrg2_core::ObjectData& odata, srrg2_core::IdContext& context) override {
      BaseType::serialize(odata, context);
      odata.setEigen("transform", this->_T);
    }

    void deserialize(ObjectData& odata, IdContext& context) override {
      BaseType::deserialize(odata, context);
      Isometry3f est = odata.getEigen<Isometry3f>("transform");
      this->setTransform(est);
    }

    void _drawImpl(ViewerCanvasPtr canvas_) const override;

  protected:
    // here we store the measurement
    Isometry3_<DualValuef> _inverse_measurement;
    Isometry3_<DualValuef> _T;
  };
} // namespace srrg2_solver_extras
