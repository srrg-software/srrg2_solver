#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <fstream>
#include <iomanip>
#include <regex>

#include "srrg_solver/solver_core/instances.h"
#include "srrg_solver/solver_core/internals/linear_solvers/instances.h"
#include "srrg_solver/solver_core/internals/linear_solvers/sparse_block_linear_solver_cholesky_csparse.h"
#include "srrg_solver/solver_core/internals/linear_solvers/sparse_block_linear_solver_cholmod_full.h"
#include "srrg_solver/solver_core/iteration_algorithm_ddl.h"
#include "srrg_solver/solver_core/iteration_algorithm_dl.h"
#include "srrg_solver/solver_core/iteration_algorithm_gn.h"
#include "srrg_solver/solver_core/iteration_algorithm_lm.h"

#include "srrg_solver/solver_core/solver.h"
#include "srrg_solver/solver_core/termination_criteria.h"
#include "srrg_solver/utils/instances.h"
#include "srrg_solver/variables_and_factors/types_3d/instances.h"
#include <srrg_config/configurable_manager.h>
#include <srrg_solver_extras/types_3d_ad/instances.h>
#include <srrg_solver_extras/types_3d_ad/se3_pose_pose_offset_quaternion_error_factor_ad.h>
#include <srrg_system_utils/parse_command_line.h>
#include <srrg_system_utils/shell_colors.h>

using namespace srrg2_core;
using namespace srrg2_solver;
using namespace srrg2_solver_extras;

using FactorType   = SE3PosePoseOffsetQuaternionErrorFactorAD;
using VariableType = VariableSE3QuaternionLeftAD;

// ia checks whether file exists
inline bool checkFile(const std::string& name_) {
  struct stat buffer;
  return (stat(name_.c_str(), &buffer) == 0);
}

// ia register types
void initTypes() {
  variables_and_factors_3d_registerTypes();
  solver_utils_registerTypes();
  solver_registerTypes();
  linear_solver_registerTypes();
  registerTypes3DAutoDiff();
}

const std::string exe_name = environ[0];
#define LOG std::cerr << exe_name << "|"

std::vector<Isometry3f> read_trajectory_file(const std::string& filename_) {
  std::ifstream is(filename_.c_str());
  if (!is.good())
    throw std::runtime_error(
      exe_name + "|ERROR, error parsing file, make sure format is #timestamp tx ty tz qx qy qz qw");
  std::vector<Isometry3f> poses;
  while (is) {
    char line[1024];
    is.getline(line, 1024);
    if (strlen(line) == 0 || line[0] == '#')
      continue;
    std::istringstream ls(line);
    double dt, tx, ty, tz, qx, qy, qz, qw;
    ls >> dt >> tx >> ty >> tz >> qx >> qy >> qz >> qw;
    Isometry3f pose = Isometry3f::Identity();
    pose.translation() << tx, ty, tz;
    Quaternionf quat = Quaternionf(qw, qx, qy, qz);
    pose.linear()    = quat.toRotationMatrix();
    poses.emplace_back(pose);
  }
  poses.shrink_to_fit();
  return poses;
}

template <typename FactorType_, typename VariableType_>
void createFactorGraph(std::vector<Isometry3f>& gt_trajectory_,
                       std::vector<Isometry3f>& estimate_trajectory_,
                       const FactorGraphPtr& graph_);
void writeTrajectory(const std::string& traj_name_, const std::vector<Isometry3f>& trajectory_);
std::shared_ptr<IterationAlgorithmBase> getIterationAlgorithm(const std::string& alg_name_);
void writeResults(const std::string& results_path_,
                  const float& rmse,
                  const std::size_t& poses_count,
                  const Eigen::Matrix4f& worlds_matrix,
                  const Eigen::Matrix4f& sensor_matrix);

int main(int argc, char** argv) {
  initTypes();

  ParseCommandLine cmd_line(argv);
  ArgumentFlag param_verbose(&cmd_line,
                             "v",
                             "verbose",
                             "verbose optimization (tells you things while they are happening)",
                             false);
  ArgumentString param_input_file_gt(
    &cmd_line, "gt", "input-file-gt", "file containing reference trajectory ", "");
  ArgumentString param_input_file_estimate(
    &cmd_line,
    "est",
    "input-file-est",
    "file containing estimate trajectory which will be aligned ",
    "");
  ArgumentString output_file(
    &cmd_line, "o", "output-file", "file where to save the output trajectory aligned", "");
  ArgumentInt num_iterations(&cmd_line, "n", "num-iterations", "num iterations", 100);
  ArgumentString solver_type(&cmd_line, "s", "solver-type", "{gn, lm, dl, ddl}", "lm");
  cmd_line.parse();

  std::shared_ptr<IterationAlgorithmBase> it_algorithm = getIterationAlgorithm(solver_type.value());

  // check if input is provided
  if (!param_input_file_gt.isSet()) {
    std::cerr << cmd_line.options() << std::endl;
    throw std::runtime_error(exe_name + "|ERROR, no input gt file specified");
  }

  if (!param_input_file_estimate.isSet()) {
    std::cerr << cmd_line.options() << std::endl;
    throw std::runtime_error(exe_name + "|ERROR, no input estimate file specified");
  }

  std::vector<Isometry3f> gt_traj       = read_trajectory_file(param_input_file_gt.value());
  std::vector<Isometry3f> estimate_traj = read_trajectory_file(param_input_file_estimate.value());

  if (gt_traj.size() != estimate_traj.size()) {
    throw std::runtime_error(exe_name +
                             "|ERROR, file size mismatch, please syncronize poses first");
  }

  LOG << "file parsed successfully, number of poses for each trajectory: " << gt_traj.size()
      << std::endl;

  FactorGraphPtr graph(new FactorGraph);
  createFactorGraph<FactorType, VariableType>(gt_traj, estimate_traj, graph);

  Solver solver;
  solver.param_algorithm.setValue(it_algorithm);
  solver.param_termination_criteria.setValue(nullptr);
  solver.param_max_iterations.pushBack(num_iterations.value());
  solver.setGraph(graph);
  solver.compute();

  const auto& stats = solver.iterationStats();
  LOG << "stats:\n\n" << stats << std::endl;
  LOG << "status: " << solver.status() << std::endl;

  const Isometry3f Xi = static_cast<VariableType*>(graph->variable(0))->estimate();
  const Isometry3f Xj = static_cast<VariableType*>(graph->variable(1))->estimate();

  float mse = 0.f;
  std::vector<Isometry3f> estimate_traj_aligned(estimate_traj.size());
  for (size_t i = 0; i < estimate_traj.size(); ++i) {
    const auto& estimate        = estimate_traj.at(i);
    const auto estimate_aligned = Xi * estimate * Xj;
    const auto gt               = gt_traj.at(i);
    const auto diff             = estimate_aligned.translation() - gt.translation();
    mse += diff.dot(diff);
    estimate_traj_aligned.at(i) = estimate_aligned;
  }
  mse /= estimate_traj.size();
  const float rmse = sqrt(mse);

  // we want cout to be readressed to txt file and LOG using cerr not to be addressed
  // therefore one can do rosrun pkg name_executable > info.txt
  std::cout << "iteration algorithm used: " << solver_type.value() << std::endl;
  std::cout << "reference trajectory: " << param_input_file_gt.value() << std::endl;
  std::cout << "to be aligned trajectory: " << param_input_file_estimate.value() << std::endl;
  if (output_file.isSet()) {
    writeTrajectory(output_file.value(), estimate_traj_aligned);
    std::cout << "aligned trajectory written on: " << output_file.value() << std::endl;
    std::string results_path =
      std::regex_replace(output_file.value(), std::regex(".txt"), "_results.csv");
    writeResults(results_path, rmse, gt_traj.size(), Xi.matrix(), Xj.matrix());
  }

  std::cout << "two worlds alignment transform:\n";
  std::cout << Xi.matrix() << std::endl << std::endl;
  std::cout << "sensor in platform transform:\n";
  std::cout << Xj.matrix() << std::endl << std::endl;

  std::cout << "alignment error MSE: " << mse << " [m] - RMSE: " << rmse << " [m]" << std::endl;
  std::cout << "number of poses used for alignment and error calculation: " << gt_traj.size()
            << std::endl;

  std::cout << "alignment error MSE: " << mse << " [m] - RMSE: " << rmse << " [m]" << std::endl;
  std::cout << "number of poses used for alignment and error calculation: " << gt_traj.size()
            << std::endl;
  return 0;
}

void writeResults(const std::string& results_path_,
                  const float& rmse,
                  const std::size_t& poses_count,
                  const Eigen::Matrix4f& worlds_matrix,
                  const Eigen::Matrix4f& sensor_matrix) {
  std::ofstream file(results_path_);
  const std::string& sep = " ";
  if (file.is_open()) {
    file << "rmse" << sep << rmse << std::endl;
    file << "\"poses count\"" << sep << poses_count << std::endl << std::endl;
    file << "\"2 worlds transform\"" << std::endl;
    for (int row = 0; row < worlds_matrix.rows(); ++row) {
      file << sep;
      for (int col = 0; col < worlds_matrix.cols(); ++col) {
        file << worlds_matrix(row, col) << sep;
      }
      file << std::endl;
    }
    file << std::endl;

    file << "\"sensor transform\"" << std::endl;
    for (int row = 0; row < sensor_matrix.rows(); ++row) {
      file << sep;
      for (int col = 0; col < sensor_matrix.cols(); ++col) {
        file << sensor_matrix(row, col) << sep;
      }
      file << std::endl;
    }

    file.close();
  }
}

void writeTrajectory(const std::string& traj_name_, const std::vector<Isometry3f>& trajectory_) {
  std::ofstream file(traj_name_);
  if (file.is_open()) {
    for (const auto& T : trajectory_) {
      file << T.translation().x() << " " << T.translation().y() << " " << T.translation().z()
           << "\n";
    }
    file.close();
  }
}

template <typename FactorType_, typename VariableType_>
void createFactorGraph(std::vector<Isometry3f>& gt_trajectory_,
                       std::vector<Isometry3f>& estimate_trajectory_,
                       const FactorGraphPtr& graph_) {
  using FactorType   = FactorType_;
  using VariableType = VariableType_;
  // X world robot in world camera
  VariableType* X_wr_in_wc = new VariableType;
  X_wr_in_wc->setGraphId(0);
  X_wr_in_wc->setEstimate(Isometry3f::Identity());
  graph_->addVariable(VariableBasePtr(X_wr_in_wc));

  VariableType* X_c_in_r = new VariableType;
  X_c_in_r->setGraphId(1);
  X_c_in_r->setEstimate(Isometry3f::Identity());
  graph_->addVariable(VariableBasePtr(X_c_in_r));

  for (size_t i = 1; i < gt_trajectory_.size(); ++i) {
    auto& curr_estimate = estimate_trajectory_[i];
    auto& curr_gt       = gt_trajectory_[i]; // our measurement

    FactorType* factor = new FactorType();
    factor->setVariableId(0, X_wr_in_wc->graphId());
    factor->setVariableId(1, X_c_in_r->graphId());
    factor->setMeasurement(curr_gt);
    factor->setTransform(curr_estimate);

    graph_->addFactor(FactorBasePtr(factor));
  }
}

std::shared_ptr<IterationAlgorithmBase> getIterationAlgorithm(const std::string& alg_name_) {
  std::shared_ptr<IterationAlgorithmBase> it_algorithm;
  if (alg_name_ == "gn") {
    it_algorithm =
      std::static_pointer_cast<IterationAlgorithmBase>(std::make_shared<IterationAlgorithmGN>());
  } else if (alg_name_ == "lm") {
    it_algorithm =
      std::static_pointer_cast<IterationAlgorithmBase>(std::make_shared<IterationAlgorithmLM>());
  } else if (alg_name_ == "dl") {
    it_algorithm =
      std::static_pointer_cast<IterationAlgorithmBase>(std::make_shared<IterationAlgorithmDL>());
  } else if (alg_name_ == "ddl") {
    it_algorithm =
      std::static_pointer_cast<IterationAlgorithmBase>(std::make_shared<IterationAlgorithmDDL>());
  } else {
    // clang-format off
    LOG << "|ERROR, iteration algorithm does not match correct name, please choose\n" 
        << "- gn \t->Gauss Newton\n"
        << "- lm \t->Lev Marqdat\n"
        << "- dl \t->Dogleg\n"
        << "- ddl\t->Double Dogleg\n";
    // clang-format on
    throw std::runtime_error("");
  }
  return it_algorithm;
}
